# Copyright (C) 2021, Open Source Integrators
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    "name": "Account Reconciliation Date",
    "summary": "Track Reconciliation Date of Payments and Invoices",
    "version": "2.0.1.0.1",
    "depends": ["account"],
    "author": "Open Source Integrators, Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/account-reconcile",
    "category": "Finance",
    "license": "AGPL-3",
    "data": ["views/account_move.xml", "views/account_payment.xml"],
    "installable": True,
}
