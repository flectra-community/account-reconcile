# Copyright 2017-20 ForgeFlow S.L. (http://www.forgeflow.com)
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).

{
    "name": "Account Partner Reconcile",
    "version": "2.0.1.0.1",
    "category": "Accounting",
    "author": "ForgeFlow, Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/account-reconcile",
    "license": "AGPL-3",
    "depends": [
        "account",
        "account_reconciliation_widget",
    ],
    "data": ["views/res_partner_view.xml"],
    "installable": True,
}
